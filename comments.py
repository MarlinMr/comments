import praw, json, psycopg2, time
from datetime import datetime
from discord_webhook import DiscordWebhook, DiscordEmbed

embedColour = 0x50bdfe

with open("/etc/reddit/norgeBot.json", 'r',encoding='utf8') as f:
    data = json.load(f)
    reddit = praw.Reddit(client_id=data["client_id"],
        client_secret=data["client_secret"],
        user_agent=data["user_agent"],
        username=data["username"],
        password=data["password"])
    sub=data["sub"]
    dbname=data["dbname"]

with open("/etc/discord/discordConfig.json", 'r', encoding='utf8') as f:
        data = json.load(f)
        webhook_url = data['webhook_livefeed']

def post_to_discord(comment):
    if(len(comment.body)<2048):
        desc = comment.body
    else:
        desc = comment.body[:2043] + "[...]"
    webhook = DiscordWebhook(url=webhook_url, username="NRK P1")
    embed = DiscordEmbed(description=desc, color=embedColour)
    embed.set_author(name=str(comment.author),url="https://www.reddit.com/u/"+str(comment.author))
    embed.add_embed_field(name="Links", value="[permalink](https://old.reddit.com" + comment.permalink + ") [context](https://old.reddit.com" + comment.permalink + "?context=1000)")
    timeStamp = time.ctime(int(comment.created_utc))+ " UTC from rpi5"
    embed.set_footer(text=timeStamp)
    webhook.add_embed(embed)
    response = webhook.execute()

subreddit = reddit.subreddit(sub)

for comment in subreddit.stream.comments():
    conn = psycopg2.connect(f"dbname={dbname}")
    cur = conn.cursor()
    cur.execute(f"SELECT author FROM users WHERE author = '{comment.author}';")
    result = cur.fetchone()
    print(datetime.fromtimestamp(comment.created_utc),comment.author,result,comment)
    if(result is None):
        query = 'BEGIN; INSERT INTO users (created, author) VALUES (%s, %s); COMMIT;'
        cur.execute(query, (int(comment.author.created_utc), str(comment.author)))
    cur.execute(f"SELECT id FROM rnorge WHERE id = '{str(comment)}';")
    result = cur.fetchone()
    if(result is None):
        try:
            query = 'BEGIN; INSERT INTO rnorge (ts, author, id, body, dt) VALUES (%s, %s, %s, %s, %s); COMMIT;'
            cur.execute(query, (int(comment.created_utc), str(comment.author), str(comment), str(comment.body), datetime.utcfromtimestamp(comment.created_utc)))
            post_to_discord(comment)
        except Exception as e:
            print(e)
